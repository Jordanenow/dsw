<?php

require_once "views/index.view.php";
include("utils/utils.php"); 
include "entity/ImagenGaleria.php";


$imagenes = array(

    new ImagenGaleria("1.jpg", "Descripcion imagen 1", 123, 1000, 2313),
    new ImagenGaleria("2.jpg", "Descripcion imagen 2", 456, 8000, 3213),
    new ImagenGaleria("3.jpg", "Descripcion imagen 3", 789, 6000, 23),
    new ImagenGaleria("4.jpg", "Descripcion imagen 4", 987, 2000, 56),
    new ImagenGaleria("5.jpg", "Descripcion imagen 5", 654, 1000, 658),
    new ImagenGaleria("6.jpg", "Descripcion imagen 6", 321, 8000, 456),
    new ImagenGaleria("7.jpg", "Descripcion imagen 7", 147, 5000, 546),
    new ImagenGaleria("8.jpg", "Descripcion imagen 8", 258, 7000, 123),
    new ImagenGaleria("9.jpg", "Descripcion imagen 9", 369, 4000, 686),
    new ImagenGaleria("10.jpg", "Descripcion imagen 10", 963, 13000, 689),
    new ImagenGaleria("11.jpg", "Descripcion imagen 11", 852, 9000, 346),
    new ImagenGaleria("12.jpg", "Descripcion imagen 12", 741, 11000, 567)
);


?>
