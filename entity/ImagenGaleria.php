<?php

class ImagenGaleria
{
  
    private $nombre;
    private $descripcion;


    private $numVisualizaciones;


    private $numLikes;


    private $numDownloads;

    const RUTA_IMAGENES_PORTFOLIO ="images/index/portfolio/";

    const RUTA_IMAGENES_GALLERY ="images/index/gallery/";




public function __construct($nombre, $descripcion, $numVisualizaciones=0, $numLikes=0, $numDownloads=0)

    {

        $this->nombre = $nombre;

        $this->descripcion = $descripcion;

        $this->numVisualizaciones = $numVisualizaciones;

        $this->numLikes = $numLikes;

        $this->numDownloads = $numDownloads;

    }

    /* Para que devuelva el nombre */
    public function getNombre(){ return $this->nombre;}

    public function getURLPortfolio() : string

    {

        return self::RUTA_IMAGENES_PORTFOLIO . $this->getNombre();

    }

    public function getURLGallery() : string

    {

        return self::RUTA_IMAGENES_GALLERY . $this->getNombre();

    }
}
